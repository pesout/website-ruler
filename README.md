# Website Ruler

Thanks to JavaScript's cursor position tracking, you can measure lots of things. My script will show you an arrow situated in the left-bottom corner - start with clicking on it.

### Functions:

- A vertical and a horizontal axis - it's intersection equal to cursor real-time position. You can show/hide it using LINES button
- Viewport and screen resolution information. You can show/hide it using "W/H INFO" button
- Measuring of difference between two points (both on X and Y axises) - it's possible to activate this feature by clicking to the first and second point consecutively. It's recommended to have the axies showed
- Real-time cursor coordinates displaying

This tool is useful when creating a page writing a raw HTML code. You can hide the elements individually and it isn't disturbing then.

To start using is necessary to add this line to your HTML file:

```html
<script src="http://rawgit.com/pesout/website-ruler/master/ruler.js"></script>
```
